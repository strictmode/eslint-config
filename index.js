// Module for react projects

module.exports = {
  extends: ["airbnb", "airbnb-typescript", "./shared.js"],
  rules: {
    // React rules
    "react/jsx-props-no-spreading": "off",
    "react/require-default-props": "off",
    "react/react-in-jsx-scope": "off",
    "react/function-component-definition": [
      "error",
      {
        namedComponents: "function-declaration",
      },
    ],
  },
}
